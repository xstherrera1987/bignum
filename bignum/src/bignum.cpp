#include "bignum.h"
#include <cstring>
#include <cstdio>
#include <climits>
#include <sstream>
#include <stdexcept>
#include <algorithm>
#include <iterator>
#include <iostream>

using std::stringstream;
using std::ostream_iterator;

#ifdef DEBUG
extern "C" {
#endif

typedef std::vector<char>::iterator iter_t;

	bignum::bignum(): repr_valid(false) {
		integral = vector<char>(0);
		fractional = vector<char>(0);
		neg = false;
	}

	bignum::bignum(const bignum &original) {
		integral = original.integral;
		fractional = original.fractional;
		neg = original.neg;
		if (original.repr_valid)
			repr = original.repr;
		else
			repr_valid = false;
	}

	/* compare against values:
	 * http://babbage.cs.qc.cuny.edu/IEEE-754/index.xhtml
	 */
	bignum::bignum(double num): repr( string() ), repr_valid(false) {
		std::cout << "N=" << num << std::endl;

		// direct cast causes truncation, change interpretation by pointer
		unsigned long int *p = (unsigned long int*) &num;
		const unsigned long int x = *p;

		// [63] 1 bit
		neg = ((x & 0x8000000000000000) == 0x8000000000000000 );
		std::cout << "NEG=" << neg << std::endl;

		// [62-52] 11 bits with bias of 1023
		unsigned long int exp = ((0x7ff0000000000000 & x) >> 52);
		long int exp_s = exp;
		short int magnitude = static_cast<short int>( exp_s - 1023 );
		std::cout << "EXP=" << magnitude << std::endl;

		// [0-51] 52 bits (mantissa, significand)
		unsigned long int frac = 0x000fffffffffffff & x;
		std::cout << "MANT=" << std::hex << frac << std::dec << std::endl
				<< std::endl;

		// Value = 2^(Exponent − Exponent Bias) × 1.Significand
		// Note the Significand must not be converted to decimal here

		// Zero value is all zeros bit pattern (also -0, with sign bit = 0)
		// eg. 0x00000.... and 0x800000...

		// +/- infinity
		// exponent is all 1's, and significand is all zeros
		// eg. 0x7ff000.... and 0xfff00000....

		// NaN (sign bit does not matter)
		// exponential field filled with ones (like infinity values), and
		// some non-zero number in the significand (its payload)

	}

	bignum::bignum(long int intg, long int frac):
			repr(string()), repr_valid(false) {
		int i_len = intg == 0 ? 0 : numdigits(intg);
		int f_len = frac == 0 ? 0 : numdigits(frac);

		if (i_len >= 1)
			integral = vector<char>(i_len);
		else
			integral = vector<char>(0);
		if (f_len >= 1)
			fractional = vector<char>(f_len);
		else
			fractional = vector<char>(0);

		if (intg < 0 || frac < 0) neg = true;
		else neg = false;

		stringstream sst;
		string s;
		if (i_len >= 1) {
			sst.str( string() );
			if (intg < 0) sst << absval(intg);
			else sst << intg;
			s = sst.str();
			reverse_copy(s.begin(), s.end() , integral.begin());
		}
		if (f_len >= 1) {
			sst.str( string() );
			if (frac < 0) sst << absval(frac);
			else sst << frac;
			s = sst.str();
			copy(s.begin(), s.end(), fractional.begin());
		}
	}

	bignum::bignum(const string &intg, const string &frac) throw
			(invalid_argument): repr(string()), repr_valid(false)  {
		if (intg.length() > INT_MAX || frac.length() > INT_MAX)
			throw invalid_argument("argument too large");
		int i_len = static_cast<int>(intg.length());
		for (unsigned int i=0; i<intg.length(); i++) {
			if ( intg[i] != '-' && (intg[i] < '0' || intg[i] > '9'))
				throw invalid_argument("non-numeric integral argument");
		}
		for (unsigned int i=0; i<frac.length(); i++) {
			if ( frac[i] != '-' && (frac[i] < '0' || frac[i] > '9'))
				throw invalid_argument("non-numeric fractional argument");
		}

		string s;
		if (i_len > 0 ) {
			if (intg[0] == '-') {
				neg = true;
				s = intg.substr(1);
			} else {
				neg = false;
				s = intg;
			}

			bignum::trim_leadzeros(s);
			if (s.length() > 0) {
				integral = vector<char>(s.length());
				reverse_copy(s.begin(), s.end(), integral.begin());
			} else {
				integral = vector<char>(0);
			}
		} else {
			integral = vector<char>(0);
			neg = false;
		}

		int f_len = static_cast<int>(frac.length());
		if (f_len > 0 ) {
			if (frac[0] == '-') {
				neg = true;
				s = frac.substr(1);
			} else {
				s = frac;
			}

			bignum::trim_trailzeros(s);
			if (s.length() > 0) {
				fractional = vector<char>(s.length());
				copy(s.begin(), s.end(), fractional.begin());
			} else {
				fractional = vector<char>(0);
			}
		} else {
			fractional = vector<char>(0);
		}
	}

	bignum& bignum::operator=(bignum const &a) {
		if (this != &a) {
			integral = a.integral;
			fractional = a.fractional;
			neg = a.neg;
			if (a.repr_valid)
				repr = a.repr;
			else
				repr_valid = false;
		}
		return *this;
	}

	bool bignum::operator==(bignum const &a) const {
		return a.neg == this->neg && a.fractional == this->fractional
				&& a.integral == this->integral;
	}

	bool bignum::operator!=(bignum const &a) const {
		return a.neg != this->neg || a.fractional != this->fractional
				|| a.integral != this->integral;
	}



	bignum bignum::operator+(bignum const &rhs) const {
		// add fractional parts then integral (+ carry if any)
		unsigned long int rop_f = rhs.fractional.size();
		unsigned long int lop_f = this->fractional.size();
		unsigned long int rop_i = rhs.integral.size();
		unsigned long int lop_i = this->integral.size();

		bool carrytointegral;
		vector<char> resfrac;
		if (rop_f == 0 || lop_f == 0) {
			// fractional result will be a simple assignment
			carrytointegral = false;
			if (rop_f == 0 && lop_f == 0) {
				resfrac = vector<char>(0);
			} else {
				if (rop_f == 0) {
					resfrac = this->fractional;
				} else {
				// lop_f == 0
					resfrac = rhs.fractional;
				}
			}
		} else {
			// add the fractional components, set carrytointegral if needed

		}

		vector<char> resintg;
		if (carrytointegral==false && (rop_i == 0 || lop_i == 0) ) {
			// integral result will be a simple assignment
			if (rop_i == 0 && lop_i == 0) {
				resintg = vector<char>(0);
			} else {
				if (rop_f == 0) {
					resintg = this->integral;
				} else {
					resintg = rhs.integral;
				}
			}
		} else {
		// should be 5 cases in this else clause
			if ( rop_i == 0 || lop_i == 0 ) {
			// carrytointegral has to be true
				if (rop_i == 0 && lop_i == 0) {
					resintg = vector<char>(1);
					resintg[1] = 1;
				} else {
					if (rop_i == 0) {
						// res = this->integral + 1
					} else {
						// res = rhs.integral + 1
					}
				}
			} else {
				// perform addition on integral components
				if (!carrytointegral) {
					// perform addition
				} else {
					// perform addition with a carry from fractional

				}
			}
		}

	/*
	 PREVIOUS IMPLEMENTATION (attempt)

 	 	// NOTE:
		// One method to right justify shorter string:
		// Determine the lengths of A and B
		// Determine which string is longer
		// Determine the number of zeros to prepend to the shorter string
		// Create a string of zeros to prepend to the shorter string
		// Concatenate the string of zeros and the shorter string

		bool carry = false;
		int sum, i;
		for (i=0; i<min; i++) {
			sum = carry ? (*a)[i]+(*b)[i] + 1 : (*a)[i]+(*b)[i];
			if (sum < 10) {
				resdig.push_back(sum);
				carry = false;
			} else {
				resdig[i] = sum % 10;
				carry = true;
			}
		}

		if (carry) {
			if (min < max) {
				while (carry) {
					sum = (*a)[i] + 1;
					if (sum < 10) {
						resdig[i] = sum;
						carry = false;
					} else {
						resdig[i] = sum % 10;
						carry = true;
					}
					i += 1;
				}
			} else {
				// TODO: append a 1 to the larger (after revising algorithm)
			}
		}
		return *this;
	*/
		// fake retval in order to test logic
		bignum fake(0);
		return fake;
	}


	const string& bignum::str() {
		if (repr_valid) {
			return repr;
		} else {
			int i_len = static_cast<int>(integral.size());
			int f_len = static_cast<int>(fractional.size());
			if (i_len == 0 && f_len == 0) {
				repr = string("0");
				return repr;
			}

			stringstream ss;
			ostream_iterator<char> ss_it (ss);
			if (neg) ss << "-";
			if (i_len > 0 && f_len > 0) {
				reverse_copy(integral.begin(), integral.end(), ss_it);
				ss << ".";
				copy(fractional.begin(), fractional.end(), ss_it);
			} else if (i_len > 0 && f_len == 0) {
				reverse_copy(integral.begin(), integral.end(), ss_it);
			} else {
				ss << "0.";
				copy(fractional.begin(), fractional.end(), ss_it);
			}

			repr = ss.str();
			repr_valid = true;
			return repr;
		}
	}

	void bignum::trim_leadzeros(string &s) {
		int len = static_cast<int>(s.length());
		if (len == 0) return;
		int i = 0;
		while (s[i] == '0') i++;
		if (i < len) {
			if (len - i > 0 && i != 0)
				s = s.substr(i,len);
			else if (i == 0)
				return;
			else
				s.clear();
		} else {
			s.clear();
		}
	}

	void bignum::trim_trailzeros(string &s) {
		int len = static_cast<int>(s.length());
		if (len == 0) return;
		int i = len-1;
		while (s[i] == '0') i--;
		if (i >= 0) {
			if (i > 0)
				s = s.substr(0,i+1);
			else if (i==len-1)
				return;
			else
				s.clear();
		} else {
			s.clear();
		}
	}

	unsigned long int bignum::absval(long int n) {
		if (n >= 0)
			return n;
		else
			return (n ^ ULONG_MAX) + 1;
	}

	int bignum::numdigits(long int arg) {
		unsigned int num=1;
		unsigned long int comp=10;
		unsigned long int n = absval(arg);
		if (n==0) return 0;
		while (comp <= n && num <= BITS ) {
			comp *= 10;
			num++;
		}
		return num;
	}

#ifdef DEBUG
}
#endif
