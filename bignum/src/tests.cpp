// NOTE: add test function invocations to main() at the end of this file
// NOTE: add test function as friend to 'START testing' section in bignum"

// declare friend functions for testing
#define TESTING 1
// prevent name mangling, in order to examine disassembly
#define DEBUG 1

#include "bignum.h"
#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib>
#include <climits>
#include <algorithm>
#include <stdexcept>

using std::cout;
using std::endl;
using std::stringstream;
using std::pair;
using std::make_pair;
using std::for_each;
using std::invalid_argument;

// NOTE: define new tests at the top

// bignum operator+(bignum const &) const;
struct TEST_op_plus_t {
	string intg_a;
	string frac_a;
	bool neg_a;
	string intg_b;
	string frac_b;
	bool neg_b;
	string result;
};
bool TEST_op_plus() {
	return false;
}

// TODO this test should not use ctor, but manipulate objects directly
// 	through friendship
// bool operator==(bignum const &) const;
// bool operator!=(bignum const &) const;
struct TEST_equality_ops_t {
	bignum a;
	bignum b;
	bool eq;
};
bool TEST_equality_ops() {
	typedef TEST_equality_ops_t data;
	vector<data> numdata;

	// START TEST_equality_ops data list
	data a = { bignum("1234","5678"), bignum("5678","1234"), false };
	numdata.push_back(a);
	data b = { bignum("0","0"), bignum("",""), true };
	numdata.push_back(b);
	data c = { bignum("-56","-36"), bignum("-56","36"), true };
	numdata.push_back(c);
	data d = { bignum("-56","-36"), bignum("56","-36"), true };
	numdata.push_back(d);
	// END test data list

	for (unsigned int i=0; i<numdata.size(); i++) {
		if ( (numdata[i].a == numdata[i].b) != numdata[i].eq) {
			cout << "TEST_equality_ops (op_eq): " << numdata[i].a.str() <<
					" == "<< numdata[i].b.str() << " should be " <<
					numdata[i].eq << endl;
			return false;
		}
		if ( (numdata[i].a != numdata[i].b) == numdata[i].eq) {
			cout << "TEST_equality_ops (op_n_eq): " << numdata[i].a.str() <<
					" != "<< numdata[i].b.str() << " should be " <<
					!numdata[i].eq << endl;
			return false;
		}
	}
	return true;
}

// TODO finish this
// bignum::bignum(double);
bool TEST_ctor_double() {
	double vals[] = {.125, -.125, 2048.0, 0.0, 3.1415926535};
	string expect[] = {".125", "-.125", "2048.0", "0.0", "3.1415926535"};

	bignum b;
	for (unsigned int i=0; i<(sizeof(vals))/sizeof(double); i++) {
		b = bignum(vals[i]);
		if (b.str() != expect[i]) {
			cout << "TEST_ctor_double: " << b.str() << " expected: "
					<< expect[i] << endl;
			return false;
		}
	}

	return true;
}

// static void trim_leadzeros(string &);
// static void trim_trailzeros(string &);
bool TEST_trimzeros() {
	string leadzeros[] = {"1", "1234", "001234", "0100","0", ""};
	string leadzeros_expect[] = {"1", "1234", "1234", "100", "", ""};

	string trailzeros[] = {"1", "1234", "1234000", "00030", "0", ""};
	string trailzeros_expect[] = {"1", "1234", "1234", "0003", "", ""};

	for (unsigned int i=0; i<(sizeof(leadzeros))/sizeof(string); i++) {
		bignum::trim_leadzeros(leadzeros[i]);
		if (leadzeros[i] != leadzeros_expect[i]) {
			cout << "TEST_trimzeros: " << leadzeros[i] << " expected: "
					<< leadzeros_expect[i] << endl;
			return false;
		}
	}

	for (unsigned int i=0; i<(sizeof(trailzeros))/sizeof(string); i++) {
		bignum::trim_trailzeros(trailzeros[i]);
		if (trailzeros[i] != trailzeros_expect[i]) {
			cout << "TEST_trimzeros: " << trailzeros[i] << " expected: "
					<< trailzeros_expect[i] << endl;
			return false;
		}
	}

	return true;
}

// bignum::bignum(string intg, string frac)
struct TEST_ctor_string_string_t {
	string intg;
	string frac;
	string repr;
};
bool TEST_ctor_string_string() {
	typedef TEST_ctor_string_string_t data;
	vector<data> numdata;

	try {
		bignum err0("34a5", "9519");
		return false;
	} catch (invalid_argument& e) {
		// do nothing it should throw
	}
	try {
		bignum err1("3435", "95a9");
		return false;
	} catch (invalid_argument& e) {
		// do nothing it should throw
	}

	// START TEST_ctor_string_string() data list
	data a = {"1998", "4321", "1998.4321"};
	numdata.push_back(a);
	data b = {"","5678", "0.5678"};
	numdata.push_back(b);
	data c = {"12754", "", "12754"};
	numdata.push_back(c);
	data d = {"-98", "6", "-98.6"};
	numdata.push_back(d);
	data e = {"98", "-6", "-98.6"};
	numdata.push_back(e);
	data f = {"", "-14159", "-0.14159"};
	numdata.push_back(f);
	data g = {"-", "14159", "-0.14159"};
	numdata.push_back(g);
	data h = {"", "", "0"};
	numdata.push_back(h);
	data i = {"0", "0", "0"};
	numdata.push_back(i);
	data j = {"", "0", "0"};
	numdata.push_back(j);
	data k = {"0", "", "0"};
	numdata.push_back(k);
	// END test data list

	for (unsigned int i=0; i < numdata.size(); i++) {
		bignum n(numdata[i].intg, numdata[i].frac);

		// check that it matches test data
		if (n.str() != numdata[i].repr) {
			cout << "TEST_ctor_string_string():" << endl;
			cout << numdata[i].intg << "," << numdata[i].frac << " as: "
					<< n.str() << " does not match" << " its expected "
					<< "representation as: " << numdata[i].repr;
			return false;
		}
	}
	return true;
}

// const string& str();
struct TEST_str_t {
	string intg;
	string frac;
	bool neg;
	string repr;
};
bool TEST_str() {
	typedef TEST_str_t data;
	vector<data> numdata;

	// START TEST_str() data list
	data a = {"8991", "4321", false,"1998.4321"};
	numdata.push_back(a);
	data b = {"","5678", false, "0.5678"};
	numdata.push_back(b);
	data c = {"45721", "", false, "12754"};
	numdata.push_back(c);
	data d = {"89", "6", true, "-98.6"};
	numdata.push_back(d);
	data e = {"", "14159", true, "-0.14159"};
	numdata.push_back(e);
	data f = {"", "", false, "0"};
	numdata.push_back(f);
	// END test data list

	string s;
	bignum n;
	data x;
	for (unsigned int i=0; i < numdata.size(); i++) {
		x = numdata[i];
		// copy test data into bignum object
		int i_len = static_cast<int>(x.intg.length());
		int f_len = static_cast<int>(x.frac.length());
		n.integral = vector<char>(i_len);
		n.fractional = vector<char>(f_len);
		if (x.intg != "")
			copy (x.intg.begin(), x.intg.end(), n.integral.begin());
		if (x.frac != "")
			copy (x.frac.begin(), x.frac.end(), n.fractional.begin());
		n.neg = x.neg;
		// force recreation of bignum::repr
		n.repr_valid = false;
		// check that it matches test data
		if (n.str() != x.repr) {
			cout << "TEST_str():" << endl;
			cout << x.intg << "," << x.frac << " as: " << n.str() << " does "
					<< "not match its expected representation as:" << x.repr;
			return false;
		}
	}
	return true;
}

// bignum(long int, long int);
struct TEST_ctor_long_long_t {
	long intg;
	long frac;
	string repr;
};
bool TEST_ctor_long_long() {
	typedef TEST_ctor_long_long_t data;
	vector<data> numdata;

	// push test data and the expected representation into vectors
	// START TEST_ctor_long_long() data list
	data a = {1987, 4321, "1987.4321"};
	numdata.push_back(a);
	data b = {0,3333,"0.3333"};
	numdata.push_back(b);
	data c = {-55,44,"-55.44"};
	numdata.push_back(c);
	data d = {0,0,"0"};
	numdata.push_back(d);
	data e = {6479,0,"6479"};
	numdata.push_back(e);
	data f = {0,-123,"-0.123"};
	numdata.push_back(f);

	stringstream ss;
	ss << LONG_MAX;
	ss << ".";
	ss << LONG_MAX;
	data g = {LONG_MAX,LONG_MAX, ss.str()};
	numdata.push_back(g);
	// END test data list

	for (unsigned int i=0; i < numdata.size(); i++) {
		bignum n(numdata[i].intg, numdata[i].frac);
		// assume bignum::str() works properly
		if (n.str() != numdata[i].repr) {
			cout << "TEST_ctor_long_long():" << endl;
			cout << numdata[i].intg << "," << numdata[i].frac << " as: "
					<< n.str() << " does not match" << " its expected "
					<< "representation as: " << numdata[i].repr;
			return false;
		}
	}
	return true;
}

// long int absval(long int n);
bool TEST_absval() {
	// the numbers used for the test
	const int nums[] = { 0, -4, 9, 10, 11, -99, -100, -1000 };
	// the absolute value of test numbers
	const unsigned int val[] = { 0, 4, 9, 10, 11, 99, 100, 1000 };
	for (unsigned int i=0; i < (sizeof(nums) / sizeof(int)); i++) {
		if (bignum::absval(nums[i]) != val[i]) {
			cout << "TEST_absval failed with n=" << nums[i] << endl;
			return false;
		}
	}
	return true;
}

// int bignum::numdigits(long int arg);
bool TEST_numdigits() {
	// the numbers used for the test
	const long nums[] = { 0, -4, 9, 10, 11, -99, -100, 1000, -1000,
			LONG_MAX / 100 ,LONG_MAX / 10, LONG_MAX };
	// the number of digits each of the test numbers requires (in base10)
	const long dig[] = { 0, 1, 1, 2, 2, 2, 3, 4, 4, 17 ,18, 19};
	for (unsigned int i=0; i < (sizeof(nums) / sizeof(long)); i++) {
		if (bignum::numdigits(nums[i]) != dig[i]) {
			cout << "TEST_numdigits failed with n=" << nums[i] << endl;
			return false;

		}
	}
	return true;
}

// NOTE: invoke new tests at bottom
int main() {
	if (!TEST_absval())
		return EXIT_FAILURE;

	if (!TEST_numdigits())
		return EXIT_FAILURE;

	if (!TEST_str())
		return EXIT_FAILURE;

	if (!TEST_ctor_long_long())
		return EXIT_FAILURE;

	if (!TEST_trimzeros())
		return EXIT_FAILURE;

	if (!TEST_ctor_string_string())
		return EXIT_FAILURE;

	/* gonna work on operator==, operator+ for now
	if (!TEST_ctor_double())
		return EXIT_FAILURE;
	*/

	if (!TEST_equality_ops())
		return EXIT_FAILURE;

	cout << "All tests OK" << endl;
	return EXIT_SUCCESS;
}
