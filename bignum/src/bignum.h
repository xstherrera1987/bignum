/** bignum represents an arbitrary precision and arbitrary magnitude real
	number. (presently it can store INT_MAX digits) each digit is stored in one
	of two vectors<char> with values between 0-9
*/

#ifndef BIGNUM_H_
#define BIGNUM_H_

#include <iostream>
#include <string>
#include <stdexcept>
#include <vector>
#include <stdexcept>

using std::ostream;
using std::string;
using std::invalid_argument;
using std::vector;

// this class is designed for 64 bit architectures
const unsigned int BITS = 64;

class bignum {
	bool neg;
	// LSD at index 0 (little-endian)
	vector<char> integral;
	// MSD at index 0 (big-endian)
	vector<char> fractional;
	// holds string representation of this number
	string repr;
	// flag indicating validity of string representation
	bool repr_valid;
public:
	// precondition: none
	// postcondition: constructs and returns a ZERO value
	bignum();
	// precondition: none
	// postcondition: constructs bignum by copying other
	bignum(const bignum&);
	// precondition:
	// postcondition: construct bignum from integers representing integral
	// 	and fractional components of a real number. if either parameter is
	//	negative this will be negative
	bignum(long int, long int);
	// precondition:
	// postcondition: construct bignum from strings representing integral
	// 	and fractional components of a real number. if either parameter has
	// 	a leading minus sign, the result is negative
	bignum(const string&, const string&) throw (invalid_argument);

	// precondition: none
	// postcondition: construct bignum from IEEE754 floating point
	//	representation
	bignum(double);

	// precondition: none
	// postcondition: copies values of a into this
	bignum& operator=(bignum const &);

	// precondition: none
	// postcondition: adds the two numbers together and returns a new value,
	// 	both parameters are unmodified
	bignum operator+(bignum const &) const;
	bignum operator-(bignum const &) const;
	bignum operator*(bignum const &) const;
	bignum operator/(bignum const &) const;

	// precondition: none
	// postcondition: tests two bignum for equality
	bool operator==(bignum const &) const;
	// precondition: none
	// postcondition: tests two bignum for inequality
	bool operator!=(bignum const &) const;

	bool operator<(bignum const &) const;
	bool operator>(bignum const &) const;
	bool operator<=(bignum const &) const;
	bool operator>=(bignum const &) const;

	// precondition: none
	// postcondition: returns textual representation of this number. return
	// 	value is only valid until next destructive operation (+=, -=, etc.)
	const string& str();

	// precondition: none
	// postcondition: returns absolute value of n
	static unsigned long int absval(long int n);

	// precondition: none
	// postcondition: returns number of base10 digits this unsigned long int
	//	will require
	static int numdigits(long int value);

	// precondition: none
	// postcondition: removes leading zeros from the argument string
	static void trim_leadzeros(string &);

	// precondition: none
	// postcondition: removes trailing zeros from the argument string
	static void trim_trailzeros(string &);

	// START testing functions
	#ifdef TESTING
	friend bool TEST_op_plus();
	friend bool TEST_ctor_double();
	friend bool TEST_equality_ops();
	friend bool TEST_ctor_string_string();
	friend bool TEST_str();
	friend bool TEST_ctor_long_long();
	friend bool TEST_absval();
	friend bool TEST_numdigits();
	#endif
	// END
};
#endif
